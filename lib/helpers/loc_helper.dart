import 'dart:convert';

import 'package:http/http.dart' as http;

const API_KEY = 'AIzaSyBicI7XeqCWg8C2cRhuwkPwiwxny9Jk_eE';

class LocHelper {
  static String generatePreview({double latitutde, double longitude}) {
    return "https://maps.googleapis.com/maps/api/staticmap?center=$latitutde,$longitude&zoom=16&size=600x300&maptype=roadmap&markers=color:red%7Clabel:A%7C$latitutde,$longitude&key=$API_KEY";
  }

  static Future<String> getAddr(double lat, double long)async{
    final url="https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&key=$API_KEY";
    final res=await http.get(url); 
    return json.decode(res.body)['results'][0]['formatted_address'];
  }
}
