import 'package:sqflite/sqflite.dart' as sql;
import 'package:path/path.dart' as path;

class dbHelper {
  static Future<sql.Database> database()async{
    final dbPath = await sql.getDatabasesPath();
    return sql.openDatabase(path.join(dbPath, 'places.db'),
        onCreate: (db, ver) {
      return db.execute(
          'create table places(id text primary key, title text, image text, loc_lat real, loc_lng real, address text)');
    }, version: 1);

  }

  static Future<void> insert(String table, Map<String, Object> data) async {
    final db=await dbHelper.database();
    await db.insert(table, data, conflictAlgorithm: sql.ConflictAlgorithm.replace);
  }

  static Future<List<Map<String, dynamic>>> getData(String table) async {
    final db=await dbHelper.database();
    return db.query(table);
  }

  static Future<void>delID(String id)async{
    final db=await dbHelper.database();
    await db.execute("delete from places where id='$id'");
  }
}
