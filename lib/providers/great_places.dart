import 'dart:io';

import 'package:flutter/foundation.dart';

import '../models/place.dart';
import '../helpers/db_helper.dart';
import '../helpers/loc_helper.dart';

class GreatPlaces with ChangeNotifier {
  List<Place> _items = [];

  List<Place> get items {
    return [..._items];
  }

  Place fetchByID(String id){
    return _items.firstWhere((place)=>place.id==id);
  }

  Future<void> addPlace(
      String title, File image, PlaceLocation pickedLoc) async {
    final addr = await LocHelper.getAddr(pickedLoc.lat, pickedLoc.long);
    final updatedLoc =
        PlaceLocation(lat: pickedLoc.lat, long: pickedLoc.long, addr: addr);
    final newPlace = Place(
      id: DateTime.now().toString(),
      img: image,
      title: title,
      loc: updatedLoc,
    );
    _items.add(newPlace);
    notifyListeners();
    dbHelper.insert('places', {
      'id': newPlace.id,
      'title': newPlace.title,
      'image': newPlace.img.path,
      'loc_lat': newPlace.loc.lat,
      'loc_lng': newPlace.loc.long,
      'address': newPlace.loc.addr,
    });
  }

  Future<void> fetchPlaces() async {
    final dataList = await dbHelper.getData('places');
    _items = dataList
        .map(
          (item) => Place(
            id: item['id'],
            title: item['title'],
            img: File(item['image']),
            loc: PlaceLocation(
              lat: item['loc_lat'],
              long: item['loc_lng'],
              addr: item['address'],
            ),
          ),
        )
        .toList();
    notifyListeners();
  }
}
