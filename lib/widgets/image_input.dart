import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as syspath;

class ImageInput extends StatefulWidget {
  final Function selecthandler;
  ImageInput(this.selecthandler);

  @override
  _ImageInputState createState() => _ImageInputState();
}

class _ImageInputState extends State<ImageInput> {
  File _storedImg;

  Future<void> _takePic() async {
    final imgFile = await ImagePicker.pickImage(
      source: ImageSource.camera,
      maxWidth: 600,
    );
    if (imgFile == null) return;
    setState(() {
      _storedImg = imgFile;
    });
    final appDir = await syspath.getApplicationDocumentsDirectory();
    final fn = path.basename(imgFile.path);
    final savedImg = await imgFile.copy('${appDir.path}/$fn');
    widget.selecthandler(savedImg);
  }

  // Future<void> _choosePic() async {
  //   final imgFile = await ImagePicker.pickImage(
  //     source: ImageSource.gallery,
  //     maxWidth: 600,
  //   );
  //   setState(() {
  //     _storedImg=imgFile;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          width: 200,
          height: 200,
          decoration: BoxDecoration(
            border: Border.all(width: 1, color: Colors.grey),
          ),
          child: _storedImg != null
              ? Image.file(
                  _storedImg,
                  fit: BoxFit.cover,
                  width: double.infinity,
                )
              : Center(
                  child: Text("<Nothing to see>"),
                ),
          alignment: Alignment.center,
        ),
        // SizedBox(
        //   width: 10,
        // ),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              FlatButton.icon(
                icon: Icon(Icons.camera),
                label: Text("Take a picture"),
                textColor: Theme.of(context).primaryColor,
                onPressed: _takePic,
              ),
              // FlatButton.icon(
              //   icon: Icon(Icons.camera_alt),
              //   label: Text("Choose a picture"),
              //   textColor: Theme.of(context).primaryColor,
              //   onPressed: _choosePic,
              // ),
            ],
          ),
        )
      ],
    );
  }
}
