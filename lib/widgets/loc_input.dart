import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

import '../helpers/loc_helper.dart';
import '../screens/map_screen.dart';

class LocationInput extends StatefulWidget {
  final Function onSelectPlace;

  LocationInput(this.onSelectPlace);

  @override
  _LocationInputState createState() => _LocationInputState();
}

class _LocationInputState extends State<LocationInput> {
  String _previewImageURL;
  void _showPrev(double lat, double lng) {
    final staticMapURL =
        LocHelper.generatePreview(latitutde: lat, longitude: lng);
    setState(() {
      _previewImageURL = staticMapURL;
    }); 
  }

  Future<void> _getCurLoc() async {
    try {
      final locData = await Location().getLocation();
    _showPrev(locData.latitude, locData.longitude);
    widget.onSelectPlace(locData.latitude, locData.longitude);
    } catch (error) {
      return;
    }
  }

  Future<void> _selectOnMap() async {
    final LatLng selectedLoc = await Navigator.of(context).push(
      MaterialPageRoute(
        fullscreenDialog: true,
        builder: (ctx) => MapScreen(
          isSelecting: true,
        ),
      ),
    );
    if (selectedLoc == null) return;
    _showPrev(selectedLoc.latitude, selectedLoc.longitude);
    widget.onSelectPlace(selectedLoc.latitude, selectedLoc.longitude);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey, width: 1),
          ),
          height: 170,
          width: double.infinity,
          alignment: Alignment.center,
          child: _previewImageURL == null
              ? Center(child: Text("<No location chosen>"))
              : Image.network(
                  _previewImageURL,
                  fit: BoxFit.cover,
                  width: double.infinity,
                ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FlatButton.icon(
              icon: Icon(Icons.location_on),
              label: Text("Current location"),
              textColor: Theme.of(context).primaryColor,
              onPressed: _getCurLoc,
            ),
            SizedBox(
              width: 20,
            ),
            FlatButton.icon(
              icon: Icon(Icons.map),
              label: Text("Select on map"),
              textColor: Theme.of(context).primaryColor,
              onPressed: _selectOnMap,
            ),
          ],
        )
      ],
    );
  }
}
