import 'dart:io';

import 'package:flutter/foundation.dart';

class PlaceLocation {
  final double lat;
  final double long;
  final String addr;
  const PlaceLocation({this.addr, @required this.lat, @required this.long});
}

class Place {
  final String id;
  final String title;
  final PlaceLocation loc;
  final File img;

  Place({
    @required this.id,
    @required this.title,
    @required this.loc,
    @required this.img,
  });
}
