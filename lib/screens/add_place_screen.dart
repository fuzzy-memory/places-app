import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../widgets/image_input.dart';
import '../widgets/loc_input.dart';
import '../providers/great_places.dart';
import '../models/place.dart';

class AddPlaceScreen extends StatefulWidget {
  static const routeName = '/add-place';

  @override
  _AddPlaceScreenState createState() => _AddPlaceScreenState();
}

class _AddPlaceScreenState extends State<AddPlaceScreen> {
  final _titleController = TextEditingController();
  File _pickedImg;
  PlaceLocation _pickedLoc;

  void _selectImg(File picked){
    _pickedImg=picked;
  }

  void _selectPlace(double lat, double lng){
    _pickedLoc=PlaceLocation(lat: lat, long: lng);
  }

  void _savedPlace(){
    if(_titleController.text.isEmpty || _pickedImg==null || _pickedLoc==null){
      return;
    }
    Provider.of<GreatPlaces>(context, listen: false).addPlace(_titleController.text , _pickedImg, _pickedLoc);
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add a place"),
      ),
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  children: <Widget>[
                    TextField(
                      decoration: InputDecoration(labelText: "Title"),
                      textCapitalization: TextCapitalization.sentences,
                      controller: _titleController,
                    ),
                    SizedBox(height: 40,),
                    ImageInput(_selectImg),
                    SizedBox(height: 40,),
                    LocationInput(_selectPlace), 
                  ],
                ),
              ),
            ),
          ),
          RaisedButton.icon(
            elevation: 0,
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            color: Theme.of(context).accentColor,
            onPressed: _savedPlace,
            icon: Icon(
              Icons.add,
              color: Colors.white,
            ),
            label: Text(
              "Add",
              style: TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }
}
