import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../models/place.dart';

class MapScreen extends StatefulWidget {
  final PlaceLocation initLoc;
  final bool isSelecting;

  MapScreen(
      {this.initLoc = const PlaceLocation(lat: 13.3525, long: 74.7928),
      this.isSelecting = false});

  static const routeName = "/maps";

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  LatLng _pickedLoc;
  void _selectLoc(LatLng pos) {
    setState(() {
      _pickedLoc = pos;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Map"),
        actions: <Widget>[
          if (widget.isSelecting)
            IconButton(
              icon: Icon(Icons.check),
              onPressed: _pickedLoc == null
                  ? null
                  : () {
                      Navigator.of(context).pop(_pickedLoc);
                    },
            )
        ],
      ),
      body: GoogleMap(
        initialCameraPosition: CameraPosition(
          target: LatLng(widget.initLoc.lat, widget.initLoc.long),
          zoom: 16,
        ),
        onTap: widget.isSelecting ? _selectLoc : null,
        markers: (_pickedLoc == null && widget.isSelecting)
            ? null
            : {
                Marker(
                  markerId: MarkerId('m1'),
                  position: _pickedLoc ??
                      LatLng(widget.initLoc.lat, widget.initLoc.long),
                ),
              },
      ),
    );
  }
}
